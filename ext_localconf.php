<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.view_statistics',
    'Visitors',
    ['Visitors' => 'show'],
    ['Visitors' => 'show']
);
