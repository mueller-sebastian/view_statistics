# View-Statistics Besucherzähler
Die View-Statistics Erweiterung zählt automatisch die Anzahl der Besucher Ihrer Webseite.
Die Anzahl der Besucher finden Sie im Backend-Modul *Pages*. Unter der Root-Seite klicken Sie auf
*Edit page properties* und im Tab *SEO* finden Sie das Feld *visitors*.

## Frontend Plugin
Die Anzahl der Besucher kann auch im Frontend mithilfe des Plugins *Visitors* angezeigt werden.
