<?php


namespace CodingMs\ViewStatistics\Controller;


use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class VisitorsController
 * @package CodingMs\ViewStatistics\Controller
 */
class VisitorsController extends ActionController
{
    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function showAction(){
        /**@var SiteFinder $siteFinder*/
        $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        $site = $siteFinder->getSiteByPageId((int)$GLOBALS['TSFE']->id);
        $rootPageId = $site->getRootPageId();

        /**@var Connection $connection*/
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('pages');
        $queryBuilder = $connection->createQueryBuilder();
        $visistorsQuery = $queryBuilder->select('visitors')
            ->from('pages')
            ->where($queryBuilder->expr()->eq('uid', $rootPageId))
            ->setMaxResults(1)
            ->execute();
        $visitors = $visistorsQuery->fetch()['visitors'];

        $this->view->assign('visitors', $visitors);

    }

}
