<?php
return [
    'frontend' => [
        'codingms/viewstatistics/middleware' => [
            'target' => \CodingMs\ViewStatistics\Middleware\CheckDataSubmissionMiddleware::class,
            'before' => [
                'typo3/cms-frontend/output-compression'
            ]
        ],
    ],
];
