<?php
declare(strict_types=1);

return [
    \CodingMs\ViewStatistics\Domain\Model\FrontendUser::class => [
        'tableName' => 'fe_users',
    ],
    \CodingMs\ViewStatistics\Domain\Model\Page::class => [
        'tableName' => 'pages'
    ],
    \CodingMs\ViewStatistics\Domain\Model\Track::class => [
        'tableName' => 'tx_viewstatistics_domain_model_track',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ]
        ]
    ]
];
